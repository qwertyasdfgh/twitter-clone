import type { ReactNode } from "react";

export function Card({
  children,
  className,
}: {
  children: ReactNode;
  className?: string;
}) {
  return (
    <div
      className={`flex z-10 flex-col gap-5 p-8 w-full rounded-md border bg-ctp-mantle/50 ${className}`}
    >
      {children}
    </div>
  );
}
