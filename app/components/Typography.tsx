import type { ReactNode } from "react";

export function Text({
  children,
  type = "",
  className = "",
}: {
  children: ReactNode;
  type?: string;
  className?: string;
}) {
  return (
    <p
      className={`${
        type === "link"
          ? "text-ctp-sky hover:text-ctp-blue"
          : type === "error"
          ? "text-ctp-red text-sm"
          : type === "subtitle"
          ? "text-ctp-subtext1 text-sm"
          : type === "success"
          ? "text-ctp-green/25"
          : "text-ctp-text"
      } ${className}`}
    >
      {children}
    </p>
  );
}

export function Title({
  children,
  className = "",
}: {
  children: ReactNode;
  className?: string;
}) {
  return <h1 className={`text-2xl font-bold ${className}`}>{children}</h1>;
}

export function SubTitle({
  children,
  className = "",
  type,
}: {
  children: ReactNode;
  className?: string;
  type?: "link";
}) {
  return (
    <h2
      className={`text-xl font-semibold ${
        type ? "text-ctp-sky hover:text-ctp-blue" : "text-ctp-text/75"
      } ${className}`}
    >
      {children}
    </h2>
  );
}
