import type { ReactNode } from "react";
import { Text } from "./Typography";

export function Button({
  type,
  children,
}: {
  type: "button" | "submit" | "reset";
  children?: ReactNode;
}) {
  return (
    <button
      className="px-6 py-2 rounded-lg border w-fit bg-ctp-crust/50 hover:bg-ctp-crust"
      type={type}
    >
      <Text>{children}</Text>
    </button>
  );
}
