import { Link, useFetcher } from "@remix-run/react";
import { Button } from "./Button";
import { Card } from "./Card";
import { FormLabel, TextArea } from "./Form";
import { SubTitle, Text } from "./Typography";
import type { PostWithRelations } from "~/utils/prisma.server";
import type { ReactNode } from "react";

export function Poster({
  username,
  name,
  pfp,
  className,
  style = "normal",
}: {
  username: string;
  name: string | null;
  pfp: string;
  className?: string;
  style?: "normal" | "compact";
}) {
  return style === "compact" ? (
    <div>
      <img
        className={`w-6 h-6 rounded-full border peer/image`}
        src={
          pfp
            ? `
          /uploads/${encodeURIComponent(pfp)}`
            : `/uploads/default.png`
        }
        width={24}
        height={24}
        alt="pfp"
      />
      <Card className="!p-3 hidden peer-hover/image:flex hover:flex !absolute !w-fit">
        <Poster username={username} pfp={pfp} name={name} />
      </Card>
    </div>
  ) : (
    <div className={`flex gap-3 items-center break-all ${className}`}>
      <img
        className="w-12 h-12 rounded-full border"
        src={
          pfp
            ? `
          /uploads/${encodeURIComponent(pfp)}`
            : `/uploads/default.png`
        }
        height={48}
        width={48}
        alt="pfp"
      />
      <div>
        {name ? (
          <>
            <SubTitle>{name}</SubTitle>
            <Link to={`/users/${username}`}>
              <Text type="link">@{username}</Text>
            </Link>
          </>
        ) : (
          <SubTitle>
            <Link to={`/users/${username}`}>
              <Text type="link">@{username}</Text>
            </Link>
          </SubTitle>
        )}
      </div>
    </div>
  );
}

export function Post({
  userId,
  post,
  topTitle,
  child,
  reply,
  enlarge,
  rootPostId,
}: {
  post: PostWithRelations;
  userId?: number;
  topTitle?: ReactNode;
  child?: boolean;
  reply?: boolean;
  enlarge?: boolean;
  rootPostId?: number;
}) {
  const fetcher = useFetcher();
  const liked = post.likes.filter((like) => like.userId === userId).length > 0;
  const reposted =
    post.reposts.filter((repost) => repost.userId === userId).length > 0;

  return (
    <>
      {post?.parentReply?.text ? (
        <>
          <Post
            post={post.parentReply}
            userId={userId}
            child={true}
            reply={true}
            rootPostId={rootPostId}
            topTitle={
              <>
                Replying to{" "}
                {post.parentReply.parentReply ? (
                  <Poster
                    style="compact"
                    username={post.parentReply.parentReply.author.username}
                    name={post.parentReply.parentReply.author.name}
                    pfp={post.parentReply.parentReply.author.pfp}
                  />
                ) : (
                  <Poster
                    style="compact"
                    username={post.parentReply.post.author.username}
                    name={post.parentReply.post.author.name}
                    pfp={post.parentReply.post.author.pfp}
                  />
                )}
              </>
            }
          />
          <div className="h-8 pl-[36px]">
            <div className="w-0 h-full border"></div>
          </div>
        </>
      ) : (
        ""
      )}
      <div
        key={post.id}
        className={`flex relative flex-col ${
          enlarge ? "text-xl" : ""
        } gap-4 transition-all hover:bg-ctp-base first:rounded-t-lg last:rounded-b-lg`}
      >
        <div
          className={` ${!child && reply ? `border-y py-4` : ""} ${
            !reply ? "!p-4" : ""
          } px-4`}
        >
          <div className="flex gap-4">
            <div className="flex flex-col items-center min-h-full">
              <img
                className="w-12 h-11 rounded-full border"
                src={`/uploads/${post.author.pfp}`}
                height={48}
                width={48}
                alt="pfp"
              />
              {child ? <div className="w-0 h-full border"></div> : ""}
            </div>
            <div className="flex relative flex-col gap-2 w-full">
              <div className="absolute right-0">
                <Text className="subtitle text-[12px] text-ctp-subtext1/30">
                  {new Date(post.createdAt).toLocaleDateString("en-fi", {
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                    hour: "numeric",
                    minute: "numeric",
                  })}
                </Text>
              </div>
              <div className="flex flex-col gap-2">
                {post.reposters?.length > 0 ? (
                  <div className="flex gap-1 items-center">
                    {post.reposters.map((user) => (
                      <Poster
                        style="compact"
                        key={user.id}
                        name={user.name}
                        username={user.username}
                        pfp={user.pfp}
                      />
                    ))}
                    <Text type="subtitle">reposted this</Text>
                  </div>
                ) : (
                  ""
                )}
                {topTitle ? (
                  <Text className="flex gap-1" type="subtitle">
                    {topTitle}
                  </Text>
                ) : (
                  ""
                )}
                <div className="flex gap-2">
                  {post.author.name ? (
                    <>
                      <SubTitle>{post.author.name}</SubTitle>
                      <Link to={`/users/${post.author.username}`}>
                        <Text type="link">@{post.author.username}</Text>
                      </Link>
                    </>
                  ) : (
                    <SubTitle>
                      <Link to={`/users/${post.author.username}`}>
                        <Text type="link">@{post.author.username}</Text>
                      </Link>
                    </SubTitle>
                  )}
                </div>
              </div>
              {!enlarge ? (
                <Link to={`/${rootPostId ? "reply" : "post"}/${post.id}`}>
                  <div>
                    <Text>{post.text}</Text>
                  </div>
                </Link>
              ) : (
                <div>
                  <Text>{post.text}</Text>
                </div>
              )}

              <div className="flex gap-2 text-sm">
                <fetcher.Form action={`/home`} method="POST">
                  <input
                    type="hidden"
                    name="intent"
                    value={liked ? "unlike" : "like"}
                  />
                  <input
                    type="hidden"
                    name={rootPostId ? "replyId" : "postId"}
                    value={post.id}
                  />
                  <button type="submit">
                    <Text type="link">
                      {liked ? "Unlike" : "Like"} ({post.likes.length})
                    </Text>
                  </button>
                </fetcher.Form>
                <fetcher.Form action={`/home`} method="POST">
                  <input
                    type="hidden"
                    name="intent"
                    value={reposted ? "unrepost" : "repost"}
                  />
                  <input
                    type="hidden"
                    name={rootPostId ? "replyId" : "postId"}
                    value={post.id}
                  />
                  <button type="submit">
                    <Text type="link">
                      {reposted ? "Unrepost" : "Repost"} ({post.reposts.length})
                    </Text>
                  </button>
                </fetcher.Form>
                <details className="space-y-2">
                  <summary className="flex cursor-pointer">
                    <Text type="link">
                      Reply ({post._count.replies}
                      {post._count.childReplies})
                    </Text>
                  </summary>
                  <Card className="absolute max-w-xs z-100">
                    <SubTitle>Reply to {post.author.username}</SubTitle>
                    <fetcher.Form
                      action={`/home`}
                      className="flex flex-col gap-3"
                      method="POST"
                    >
                      <input type="hidden" name="intent" value={"reply"} />
                      {rootPostId ? (
                        <>
                          <input
                            type="hidden"
                            name="postId"
                            value={rootPostId}
                          />
                          <input type="hidden" name="replyId" value={post.id} />
                        </>
                      ) : (
                        <input type="hidden" name="postId" value={post.id} />
                      )}
                      <FormLabel>
                        <Text>Reply body</Text>
                        <TextArea name="reply" />
                      </FormLabel>
                      <Button type="submit">Post</Button>
                    </fetcher.Form>
                  </Card>
                </details>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
