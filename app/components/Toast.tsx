import type { ReactNode } from "react";
import { SubTitle, Text } from "./Typography";

export function Toast({ children }: { children: ReactNode }) {
  return (
    <>
      <input className="peer/toast" type="checkbox" id="toast-toggle" hidden />
      <div className="fixed right-10 bottom-10 z-10 p-4 w-80 rounded-lg border shadow-lg peer-checked/toast:hidden bg-ctp-base">
        <div className="relative">
          <label
            className="absolute right-0 text-gray-300 cursor-pointer hover:text-gray-400"
            htmlFor="toast-toggle"
          >
            X
          </label>
          <SubTitle>Alert</SubTitle>
          <Text>{children}</Text>
        </div>
      </div>
    </>
  );
}
