import type { ReactNode } from "react";

export function FormLabel({ children }: { children: ReactNode }) {
  return <label className="flex flex-col gap-2">{children}</label>;
}

export function FormInput({ type, name }: { type: string; name: string }) {
  return (
    <input
      className={`file:bg-transparent file:border-0 ${
        type === "file" ? "p-4 cursor-pointer" : "p-2"
      } file:bg-sky-100 file:hover:bg-sky-100/75 file:text-gray-600/75 file:p-2 file:rounded-lg bg-ctp-crust/50 text-ctp-subtext0 rounded-lg border`}
      type={type}
      name={name}
      accept={type === "file" ? "image/*" : undefined}
      placeholder="Hello world..."
    />
  );
}

export function TextArea({
  name,
  placeholder = "Hello",
}: {
  name: string;
  placeholder?: string;
}) {
  return (
    <textarea
      className="p-4 w-full rounded-lg border bg-ctp-crust/50 text-ctp-subtext0"
      placeholder={placeholder}
      name={name}
    ></textarea>
  );
}
