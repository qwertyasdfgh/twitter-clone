import { createCookieSessionStorage } from "@remix-run/node";

export const { getSession, commitSession, destroySession } =
  createCookieSessionStorage({
    cookie: {
      path: "/",
      secrets: [process.env.TC_SECRETS || ""],
      sameSite: "strict",
    },
  });
