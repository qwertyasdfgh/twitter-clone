import type { Prisma } from "@prisma/client";
import { PrismaClient } from "@prisma/client";

let prisma: PrismaClient;
export type PostWithRelations = Prisma.PostGetPayload<{
  include: { likes: true; author: true; reposts: true };
}>;
export type UserWithRelations = Prisma.UserGetPayload<{
  include: {
    followedBy: true;
    following: true;
    posts: {
      include: {
        author: true;
        likes: true;
        reposts: true;
      };
    };
    reposts: {
      include: {
        author: true;
        likes: true;
        reposts: true;
      };
    };
    likes: {
      include: {
        author: true;
        likes: true;
        reposts: true;
      };
    };
    _count: {
      select: {
        followedBy: true;
        following: true;
      };
    };
  };
}>;
declare global {
  var __db: PrismaClient | undefined;
}

if (process.env.NODE_ENV === "production") {
  prisma = new PrismaClient();
  prisma.$connect();
} else {
  if (!global.__db) {
    global.__db = new PrismaClient();
    global.__db.$connect();
  }
  prisma = global.__db;
}

export { prisma };
