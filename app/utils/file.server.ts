import {
  unstable_composeUploadHandlers,
  unstable_createFileUploadHandler,
  unstable_createMemoryUploadHandler,
} from "@remix-run/node";

export const profilePictureUploadHandler = unstable_composeUploadHandlers(
  unstable_createFileUploadHandler({
    directory: "public/uploads",
    maxPartSize: 1000000,
    file({ filename }) {
      return filename;
    },
    filter({ contentType }) {
      return contentType.includes("image");
    },
  }),

  unstable_createMemoryUploadHandler()
);
