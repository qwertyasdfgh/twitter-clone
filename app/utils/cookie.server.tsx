import { createCookie } from "@remix-run/node";

export const theme = createCookie("theme");
