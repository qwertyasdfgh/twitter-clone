import {
  json,
  redirect,
  type LinksFunction,
  type LoaderFunction,
  type LoaderFunctionArgs,
} from "@remix-run/node";
import {
  Link,
  Links,
  LiveReload,
  Meta,
  NavLink,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
} from "@remix-run/react";

import styles from "./tailwind.css";
import { commitSession, getSession } from "./utils/session.server";
import { SubTitle } from "./components/Typography";
import { Toast } from "./components/Toast";
import { prisma } from "./utils/prisma.server";
import { Poster } from "./components/Post";
import { theme } from "./utils/cookie.server";

export const links: LinksFunction = () => [{ rel: "stylesheet", href: styles }];

export interface RootLoaderTypes {
  id: number;
  username: string;
  name: string;
  pfp: string;
  toast: string;
  theme: "latte" | "macchiato" | "mocha" | "frappe" | "none";
}

export async function loader({ request }: LoaderFunctionArgs) {
  const cookieHeader = request.headers.get("Cookie");
  const session = await getSession(cookieHeader);
  const message = session.get("globalMessage") || null;
  const themeCookie = (await theme.parse(cookieHeader)) || {};

  if (session.has("userId")) {
    const data = await prisma.user.findUnique({
      where: {
        id: session.get("userId"),
      },
      select: {
        id: true,
        username: true,
        name: true,
        pfp: true,
      },
    });

    if (!data) {
      return redirect("/logout");
    }

    return json(
      {
        id: data.id,
        username: data.username,
        name: data.name,
        pfp: data.pfp,
        theme: themeCookie.theme,
        toast: message,
      },
      {
        headers: {
          "Set-Cookie": await commitSession(session),
        },
      }
    );
  }

  return json(
    {
      toast: message,
      theme: themeCookie.theme,
    },
    {
      headers: {
        "Set-Cookie": await commitSession(session),
      },
    }
  );
}

export default function App() {
  const data = useLoaderData<LoaderFunction>();

  return (
    <html
      className={
        data.theme === "latte"
          ? "ctp-latte"
          : data.theme === "frappe"
          ? "ctp-frappe"
          : data.theme === "macchiato"
          ? "ctp-macchiato"
          : data.theme === "mocha"
          ? "ctp-mocha"
          : data.theme === "none"
          ? ""
          : "ctp-latte"
      }
      lang="en"
    >
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="min-h-screen bg-ctp-base text-ctp-text">
        <div className="container flex flex-col gap-20 m-8 lg:px-40 mx-auto xl:flex-row">
          <div className="flex mx-auto justify-center flex-grow w-80">
            <nav className="flex w-full flex-col gap-5">
              {!data?.id ? (
                <>
                  <NavItem text={`Login`} to={`/login`} />
                  <NavItem text={`Register`} to={`/register`} />
                </>
              ) : (
                <>
                  <NavItem text={`Home`} to={`home`} />
                  <NavItem text={`Settings`} to={`settings`} />
                  <Poster
                    className="justify-center"
                    username={data.username}
                    name={data.name}
                    pfp={data.pfp}
                  />
                </>
              )}
            </nav>
          </div>
          <div className="flex flex-col gap-5 flex-grow-[4] w-full">
            <Outlet />
          </div>
          <div className="flex mx-auto justify-center flex-grow w-80">
            <div>
              <div className="hover:bg-ctp-blue/5 rounded-lg transition-all p-4">
                <Link className="flex flex-col gap-5 items-center" to={"/"}>
                  <img src="/logo.png" alt="logo" width={50} />
                  <SubTitle>Twitter clone</SubTitle>
                </Link>
              </div>
            </div>
          </div>
          {data?.toast ? <Toast>{data.toast}</Toast> : ""}
        </div>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}

function NavItem({ to, text }: { to: string; text: string }) {
  return (
    <NavLink
      className={({ isActive, isPending }) =>
        isPending
          ? "pending"
          : isActive
          ? "bg-ctp-surface0 rounded-lg font-black"
          : ""
      }
      to={to}
    >
      <div className="px-4 w-full py-2 text-center rounded-lg border bg-ctp-surface0/40 hover:bg-ctp-surface2/25">
        <span>{text}</span>
      </div>
    </NavLink>
  );
}
