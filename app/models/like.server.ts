import { prisma } from "~/utils/prisma.server";
import { getPostById } from "./post.server";
import { getReplyById, reply } from "./reply.server";

interface PostErrors {
  postId: string | undefined;
  like: string | undefined;
}

interface ReplyErrors {
  replyId: string | undefined;
  reply: string | undefined;
}

export async function like(
  postId: number,
  id: number,
  replyId: number | undefined
) {
  if (replyId) {
    const errors: ReplyErrors = {
      replyId: undefined,
      reply: undefined,
    };

    const post = await getReplyById(replyId);
    if (!post) {
      errors.replyId = "No reply with that id";
    }

    const reply = await prisma.like.findFirst({
      where: {
        userId: id,
        replyId: replyId,
      },
    });
    if (reply) {
      errors.reply = "You have already liked that reply";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const replyQ = await prisma.like.create({
      data: {
        replyId: replyId,
        userId: id,
      },
    });

    if (!replyQ) {
      errors.reply = "Something went wrong liking the post";
      return errors;
    }

    return errors;
  } else {
    const errors: PostErrors = {
      postId: undefined,
      like: undefined,
    };

    const post = await getPostById(postId);
    if (!post) {
      errors.postId = "No post with that id";
    }

    const like = await prisma.like.findFirst({
      where: {
        userId: id,
        postId: postId,
      },
    });
    if (like) {
      errors.like = "You have already liked that post";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const likeQ = await prisma.like.create({
      data: {
        postId: postId,
        userId: id,
      },
    });

    if (!likeQ) {
      errors.like = "Something went wrong liking the post";
      return errors;
    }

    return errors;
  }
}

export async function unLike(
  postId: number,
  id: number,
  replyId: number | undefined
) {
  if (replyId) {
    const errors: ReplyErrors = {
      replyId: undefined,
      reply: undefined,
    };

    const reply = await getReplyById(replyId);
    if (!reply) {
      errors.replyId = "No post with that id";
    }

    const like = await prisma.like.findFirst({
      where: {
        userId: id,
        replyId: replyId,
      },
    });
    if (!like) {
      errors.reply = "Like doesn't exist";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const replyQ = await prisma.like.delete({
      where: {
        id: like.id,
      },
    });

    if (!replyQ) {
      errors.reply = "Something went wrong unliking the post";
      return errors;
    }

    return errors;
  } else {
    const errors: PostErrors = {
      postId: undefined,
      like: undefined,
    };

    const post = await getPostById(postId);
    if (!post) {
      errors.postId = "No post with that id";
    }

    const like = await prisma.like.findFirst({
      where: {
        userId: id,
        postId: postId,
      },
    });
    if (!like) {
      errors.like = "Like doesn't exist";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const likeQ = await prisma.like.delete({
      where: {
        id: like.id,
      },
    });

    if (!likeQ) {
      errors.like = "Something went wrong unliking the post";
      return errors;
    }

    return errors;
  }
}
