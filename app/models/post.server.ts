import { prisma } from "~/utils/prisma.server";
import type { Post } from "@prisma/client";

export async function getAllPosts() {
  const posts = await prisma.post.findMany({
    orderBy: {
      id: "desc",
    },
    include: {
      author: {
        select: {
          username: true,
          name: true,
          pfp: true,
        },
      },
      likes: true,
    },
  });

  return posts;
}

export async function getPostById(id: number) {
  const post = await prisma.post.findUnique({
    where: {
      id: id,
    },
  });

  return post;
}

export function validatePostBody(body: string) {
  if (!body) {
    return "Post body can't be empty";
  }
}

export async function createPost(body: Post["text"], id: number) {
  const errors: {
    body: string | undefined;
  } = {
    body: validatePostBody(body),
  };

  if (Object.values(errors).some(Boolean)) {
    return errors;
  }

  const post = await prisma.post.create({
    data: {
      text: body,
      userId: id,
    },
  });

  if (!post) {
    errors.body = "Something went wrong creating the post";
    return errors;
  }

  return errors;
}
