import { prisma } from "~/utils/prisma.server";
import { getPostById, validatePostBody } from "./post.server";
import type { Reply } from "@prisma/client";

export async function getReplyById(id: number) {
  const reply = await prisma.reply.findUnique({
    where: {
      id: id,
    },
  });

  return reply;
}

export async function reply(
  body: Reply["text"],
  postId: Reply["postId"],
  replyId: Reply["parentReplyId"] = null,
  userId: Reply["userId"]
) {
  const errors: {
    body: string | undefined;
    post: string | undefined;
    reply: string | undefined;
  } = {
    body: validatePostBody(body),
    post: undefined,
    reply: undefined,
  };

  if (replyId && !(await getReplyById(replyId))) {
    errors.reply = "Reply not found";
  }

  if (!(await getPostById(postId))) {
    errors.post = "Post not found";
  }

  if (Object.values(errors).some(Boolean)) {
    return errors;
  }

  const post = await prisma.reply.create({
    data: {
      text: body,
      userId: userId,
      postId: postId,
      parentReplyId: replyId,
    },
  });

  if (!post) {
    errors.reply = "Something went wrong creating the post";
  }

  return errors;
}
