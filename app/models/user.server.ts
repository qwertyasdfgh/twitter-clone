import { prisma } from "~/utils/prisma.server";
import type { User } from "@prisma/client";
import { unlink } from "node:fs";
import { redirect } from "@remix-run/node";
import * as argon2 from "argon2";

export async function getUserByName(username: string) {
  const user = await prisma.user.findUnique({
    where: {
      username: username,
    },
  });

  return user;
}

export async function getUsers(limit: number = 5) {
  const users = await prisma.user.findMany({
    select: {
      id: true,
      username: true,
      name: true,
      pfp: true,
    },
    take: limit,
  });

  return users;
}

export async function getUserById(id: number) {
  const user = await prisma.user.findUnique({
    where: {
      id: id,
    },
  });

  return user;
}

export async function userExists(username: string) {
  return (await getUserByName(username)) ? true : false;
}

export interface UserErrors {
  username: string | undefined;
  name: string | undefined;
  desc: string | undefined;
  opassword: string | undefined;
  npassword: string | undefined;
  pfp: string | undefined;
}

export async function followUser(followId: number, id: number) {
  const errors: { followId: string | undefined; follow: string | undefined } = {
    followId: undefined,
    follow: undefined,
  };

  if (!(await getUserById(followId))) {
    errors.followId = "User does not exist";
  }

  if (Object.values(errors).some(Boolean)) {
    return errors;
  }

  const follow = await prisma.user.update({
    where: {
      id: id,
    },
    data: {
      following: {
        connect: { id: followId },
      },
    },
  });

  if (!follow) {
    errors.follow = "Something went wrong";
  }

  return errors;
}

export async function unFollowUser(followId: number, id: number) {
  const errors: { followId: string | undefined; unfollow: string | undefined } =
    {
      followId: undefined,
      unfollow: undefined,
    };

  if (!(await getUserById(followId))) {
    errors.followId = "User does not exist";
  }

  if (Object.values(errors).some(Boolean)) {
    return errors;
  }

  const unFollow = await prisma.user.update({
    where: {
      id: id,
    },
    data: {
      following: {
        disconnect: { id: followId },
      },
    },
  });

  if (!unFollow) {
    errors.unfollow = "Something went wrong";
  }

  return errors;
}

export async function editUser(
  data: {
    intent: string;
    username: string;
    name: string;
    desc: string;
    opassword: string;
    npassword: string;
    pfp: {
      filepath: string;
      name: string;
    };
  },
  id: number
) {
  let errors: UserErrors = {
    username: undefined,
    name: undefined,
    desc: undefined,
    opassword: undefined,
    npassword: undefined,
    pfp: undefined,
  };

  switch (data.intent) {
    case "username":
      errors.username = await validateUsername(data.username);

      if (Object.values(errors).some(Boolean)) {
        return { errors: errors };
      }

      const username = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          username: data.username,
        },
      });

      if (!username) {
        errors.username = "Something went wrong";
      }

      return { username: username.username, errors: errors };
    case "name":
      if (!data.name) {
        errors.name = "Please enter a valid name";
      }

      if (data.name.length > 32) {
        errors.name = "Too many characters";
      }

      if (Object.values(errors).some(Boolean)) {
        return { errors: errors };
      }

      const name = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          name: data.name,
        },
      });

      if (!name) {
        errors.name = "Something went wrong";
      }

      return { errors: errors };
    case "desc":
      if (!data.desc) {
        errors.desc = "Please enter a valid description";
      }

      if (Object.values(errors).some(Boolean)) {
        return { errors: errors };
      }

      const desc = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          desc: data.desc,
        },
      });

      if (!desc) {
        errors.desc = "Something went wrong";
      }

      return { errors: errors };
    case "password":
      if (!data.opassword) {
        errors.opassword = "Please enter a valid password";
      }

      if (!data.npassword) {
        errors.npassword = "Please enter a valid password";
      }

      const user = await getUserById(id);

      if (!user) {
        return redirect("/logout");
      }

      if (!(await verifyPassword(data.opassword, user.password))) {
        errors.opassword = "Old password doesn't match";
      }

      if (Object.values(errors).some(Boolean)) {
        return { errors: errors };
      }

      const pw = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          password: await createPasswordHash(data.npassword),
        },
      });

      if (!pw) {
        errors.opassword = "Something went wrong";
      }

      return { errors: errors };
    case "pfp":
      if (!data.pfp.filepath) {
        errors.pfp = "Failed saving image";
      }

      if (Object.values(errors).some(Boolean)) {
        return { errors: errors };
      }

      const userpfp = await getUserById(id);
      if (!userpfp) {
        return redirect("/logout");
      }

      if (userpfp.pfp !== "default.png") {
        unlink(`public/uploads/${userpfp.pfp}`, (err) => {
          if (err) console.log(err);
        });
      }

      const pfp = await prisma.user.update({
        where: {
          id: id,
        },
        data: {
          pfp: data.pfp.name,
        },
      });

      if (!pfp) {
        errors.pfp = "Failed saving image";
      }

      return { errors: errors };
    default:
      return { errors: errors };
  }
}

async function verifyPassword(password: string, passwordHash: string) {
  return await argon2.verify(passwordHash, password);
}

export async function checkUserLogin(username: string, password: string) {
  const user = await getUserByName(username);
  if (!user || !(await verifyPassword(password, user.password))) {
    return { error: "Invalid credentials" };
  }

  return { id: user.id, username: user.username, error: "" };
}

async function validatePassword(password: string) {
  if (!password) {
    return "Enter a password";
  }
}

async function validateRepeatPassword(
  password: string,
  repeatPassword: string
) {
  if (password !== repeatPassword) {
    return "Passwords need to match";
  }
}

async function validateUsername(username: string) {
  if (!username) {
    return "Enter a username";
  }

  if (username.length > 16) {
    return "Too many characters";
  }

  if (await userExists(username)) {
    return "Username taken";
  }
}

async function createPasswordHash(password: string) {
  return await argon2.hash(password);
}

export async function createUser(
  username: User["username"],
  password: User["password"],
  repeatPassword: User["password"]
) {
  let errors: {
    username: string | undefined;
    password: string | undefined;
    rpassword: string | undefined;
  } = {
    username: await validateUsername(username),
    password: await validatePassword(password),
    rpassword: await validateRepeatPassword(password, repeatPassword),
  };

  if (Object.values(errors).some(Boolean)) {
    return { errors: errors };
  }

  const user = await prisma.user.create({
    data: {
      username: username,
      password: await createPasswordHash(password),
      pfp: "default.png",
    },
  });

  if (!user) {
    errors.username = "Failed to create user";
    return { errors: errors };
  }

  return { id: user.id, username: user.username, errors: errors };
}
