import { prisma } from "~/utils/prisma.server";
import { getPostById } from "./post.server";
import { getReplyById } from "./reply.server";

export async function repost(
  postId: number,
  id: number,
  replyId: number | undefined
) {
  if (replyId) {
    const errors = {};

    const reply = await getReplyById(replyId);

    if (!reply) {
      errors.repost = "Post doesn't exist";
    }

    const repost = await prisma.repost.findFirst({
      where: {
        replyId: replyId,
        userId: id,
      },
    });

    if (repost) {
      errors.repost = "You have already reposted that reply";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const repostQ = await prisma.repost.create({
      data: {
        userId: id,
        replyId: replyId,
      },
    });

    if (!repostQ) {
      errors.repost = "Something went wrong";
    }

    return errors;
  } else {
    const errors = {};

    const post = await getPostById(postId);

    if (!post) {
      errors.repost = "Post doesn't exist";
    }

    const repost = await prisma.repost.findFirst({
      where: {
        postId: postId,
        userId: id,
      },
    });

    if (repost) {
      errors.repost = "You have already reposted that post";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const repostQ = await prisma.repost.create({
      data: {
        userId: id,
        postId: postId,
      },
    });

    if (!repostQ) {
      errors.repost = "Something went wrong";
    }

    return errors;
  }
}

export async function unRepost(
  postId: number,
  id: number,
  replyId: number | undefined
) {
  if (replyId) {
    const errors = {};

    const reply = await getReplyById(replyId);
    if (!reply) {
      errors.repost = "Reply doesn't exist";
    }

    const repost = await prisma.repost.findFirst({
      where: {
        userId: id,
        replyId: replyId,
      },
    });

    if (!repost) {
      errors.repost = "Repost doesn't exist";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const repostQ = await prisma.repost.delete({
      where: {
        id: repost.id,
      },
    });

    if (!repostQ) {
      errors.repost = "Something went wrong";
    }

    return errors;
  } else {
    const errors = {};

    const post = await getPostById(postId);
    if (!post) {
      errors.repost = "Post doesn't exist";
    }

    const repost = await prisma.repost.findFirst({
      where: {
        userId: id,
        postId: postId,
      },
    });

    if (!repost) {
      errors.repost = "Repost doesn't exist";
    }

    if (Object.values(errors).some(Boolean)) {
      return errors;
    }

    const repostQ = await prisma.repost.delete({
      where: {
        id: repost.id,
      },
    });

    if (!repostQ) {
      errors.repost = "Something went wrong";
    }

    return errors;
  }
}
