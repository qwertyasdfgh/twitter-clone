import {
  redirect,
  type ActionFunction,
  type ActionFunctionArgs,
  type LoaderFunctionArgs,
  type MetaFunction,
} from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import { Button } from "~/components/Button";
import { Card } from "~/components/Card";
import { FormInput, FormLabel } from "~/components/Form";
import { Text, Title } from "~/components/Typography";
import { checkUserLogin } from "~/models/user.server";
import { commitSession, getSession } from "~/utils/session.server";

export const meta: MetaFunction<typeof loader> = () => {
  return [{ title: `Login | Twitter Clone` }];
};

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (session.has("userId")) {
    return redirect("/");
  }

  return null;
}

export async function action({ request }: ActionFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (session.has("userId")) {
    return redirect("/");
  }

  const formData = await request
    .formData()
    .then((data) => Object.fromEntries(data));

  const { error, id, username } = await checkUserLogin(
    String(formData.username),
    String(formData.password)
  );

  if (error) {
    return error;
  }

  session.set("userId", id);
  session.set("username", username);
  session.flash("globalMessage", `Logged in to @${username}`);

  return redirect("/", {
    headers: {
      "Set-Cookie": await commitSession(session),
    },
  });
}

export default function Login() {
  const error = useActionData<ActionFunction>();

  return (
    <div className="flex flex-col gap-5">
      <Title>Login</Title>
      <Card>
        <Form className="flex flex-col gap-3" method="POST">
          <FormLabel>
            <Text>Username</Text> <FormInput type="text" name="username" />{" "}
            <Text type="error">{error ? error : ""}</Text>
          </FormLabel>
          <FormLabel>
            <Text>Password</Text> <FormInput type="password" name="password" />{" "}
            <Text type="error">{error ? error : ""}</Text>
          </FormLabel>
          <Button type="submit">Login</Button>
        </Form>
      </Card>
    </div>
  );
}
