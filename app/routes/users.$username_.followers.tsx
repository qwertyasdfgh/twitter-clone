import type { LoaderFunction, LoaderFunctionArgs } from "@remix-run/node";
import { useLoaderData, useParams } from "@remix-run/react";
import { Card } from "~/components/Card";
import { Poster } from "~/components/Post";
import { Title } from "~/components/Typography";
import { prisma } from "~/utils/prisma.server";

export async function loader({ params }: LoaderFunctionArgs) {
  const followers = await prisma.user.findMany({
    where: {
      following: {
        some: {
          username: params.username,
        },
      },
    },
    select: {
      id: true,
      username: true,
      name: true,
      pfp: true,
    },
  });

  return followers;
}

export default function UserFollowers() {
  const data = useLoaderData<LoaderFunction>();
  const params = useParams();

  return (
    <div className="flex flex-col gap-5">
      <Title>
        User <em>{params.username}</em> followers
      </Title>
      <Card>
        {data.map((follower) => (
          <Poster
            key={follower.id}
            username={follower.username}
            name={follower.name}
            pfp={follower.pfp}
          />
        ))}
      </Card>
    </div>
  );
}
