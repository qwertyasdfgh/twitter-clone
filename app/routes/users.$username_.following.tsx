import type { LoaderFunction, LoaderFunctionArgs } from "@remix-run/node";
import { useLoaderData, useParams } from "@remix-run/react";
import { Card } from "~/components/Card";
import { Poster } from "~/components/Post";
import { Title } from "~/components/Typography";
import { prisma } from "~/utils/prisma.server";

export async function loader({ params }: LoaderFunctionArgs) {
  const followed = await prisma.user.findMany({
    where: {
      followedBy: {
        some: {
          username: params.username,
        },
      },
    },
    select: {
      id: true,
      username: true,
      name: true,
      pfp: true,
    },
  });

  console.log(followed);

  if (!followed) {
    throw Error("Not found");
  }

  return followed;
}

export default function UserFollowed() {
  const data = useLoaderData<LoaderFunction>();
  const params = useParams();

  return (
    <div className="flex flex-col gap-5">
      <Title>
        User <em>{params.username}</em> follows
      </Title>
      <Card>
        {data.map((follower) => (
          <Poster
            key={follower.id}
            username={follower.username}
            name={follower.name}
            pfp={follower.pfp}
          />
        ))}
      </Card>
    </div>
  );
}
