import type { LoaderFunction, MetaFunction } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { Card } from "~/components/Card";
import { Poster } from "~/components/Post";
import { Title } from "~/components/Typography";
import { getUsers } from "~/models/user.server";
import type { UserWithRelations } from "~/utils/prisma.server";

export const meta: MetaFunction<typeof loader> = () => {
  return [{ title: `Users | Twitter Clone` }];
};

export async function loader() {
  return await getUsers(10);
}

export default function UsersIndex() {
  const data: UserWithRelations[] = useLoaderData<LoaderFunction>();

  return (
    <>
      <Title>Users</Title>
      <Card>
        {data.map((user) => (
          <Poster
            key={user.id}
            username={user.username}
            name={user.name}
            pfp={user.pfp}
          />
        ))}
      </Card>
    </>
  );
}
