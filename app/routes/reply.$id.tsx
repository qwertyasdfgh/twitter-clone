import {
  json,
  type LoaderFunction,
  type LoaderFunctionArgs,
  type ActionFunction,
  type MetaFunction,
} from "@remix-run/node";
import {
  Form,
  useActionData,
  useFetcher,
  useLoaderData,
  useRouteLoaderData,
} from "@remix-run/react";
import { Card } from "~/components/Card";
import { SubTitle, Text, Title } from "~/components/Typography";
import type { PostWithRelations } from "~/utils/prisma.server";
import { prisma } from "~/utils/prisma.server";
import { Post, Poster } from "~/components/Post";
import type { RootLoaderTypes } from "~/root";
import { Button } from "~/components/Button";
import { FormLabel, TextArea } from "~/components/Form";

export const meta: MetaFunction<typeof loader> = ({ data, error }) => {
  return [{ title: `${error ? "Not Found" : data.text} | Twitter Clone` }];
};

export async function loader({ params }: LoaderFunctionArgs) {
  const reply = await prisma.reply.findUnique({
    where: {
      id: Number(params.id),
    },
    select: {
      id: true,
      userId: true,
      text: true,
      author: {
        select: {
          id: true,
          username: true,
          name: true,
          pfp: true,
        },
      },
      likes: {
        select: {
          id: true,
          userId: true,
        },
      },
      reposts: {
        select: {
          id: true,
          userId: true,
        },
      },
      parentReply: {
        select: {
          id: true,
          text: true,
          createdAt: true,
          parentReply: {
            select: {
              id: true,
              text: true,
              createdAt: true,
              author: {
                select: {
                  id: true,
                  username: true,
                  name: true,
                  pfp: true,
                },
              },
              post: {
                include: {
                  author: {
                    select: {
                      id: true,
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              parentReply: {
                select: {
                  author: {
                    select: {
                      id: true,
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                  post: {
                    include: {
                      author: {
                        select: {
                          id: true,
                          username: true,
                          name: true,
                          pfp: true,
                        },
                      },
                    },
                  },
                },
              },
              likes: {
                select: {
                  id: true,
                  userId: true,
                },
              },
              reposts: {
                select: {
                  id: true,
                  userId: true,
                },
              },
              _count: {
                select: {
                  childReplies: true,
                },
              },
            },
          },
          author: {
            select: {
              id: true,
              username: true,
              name: true,
              pfp: true,
            },
          },
          likes: {
            select: {
              id: true,
              userId: true,
            },
          },
          post: {
            select: {
              id: true,
              text: true,
              createdAt: true,
              likes: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              reposts: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              author: {
                select: {
                  id: true,
                  username: true,
                  name: true,
                  pfp: true,
                },
              },
              _count: {
                select: {
                  replies: true,
                },
              },
            },
          },
          reposts: {
            select: {
              id: true,
              userId: true,
            },
          },
          _count: {
            select: {
              childReplies: true,
            },
          },
        },
      },
      post: {
        select: {
          id: true,
          text: true,
          createdAt: true,
          likes: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          reposts: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          author: {
            select: {
              id: true,
              username: true,
              name: true,
              pfp: true,
            },
          },
          _count: {
            select: {
              replies: {
                where: {
                  parentReplyId: null,
                },
              },
            },
          },
        },
      },
      childReplies: {
        select: {
          id: true,
          text: true,
          createdAt: true,
          likes: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          author: {
            select: {
              id: true,
              username: true,
              name: true,
              pfp: true,
            },
          },
          reposts: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          post: {
            select: {
              id: true,
              text: true,
              createdAt: true,
              likes: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              reposts: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              author: {
                select: {
                  id: true,
                  username: true,
                  name: true,
                  pfp: true,
                },
              },
              _count: {
                select: {
                  replies: true,
                },
              },
            },
          },
          _count: {
            select: {
              childReplies: true,
            },
          },
        },
        orderBy: {
          likes: {
            _count: "desc",
          },
        },
      },
      createdAt: true,
      _count: {
        select: {
          childReplies: true,
        },
      },
    },
  });

  if (!reply) {
    throw Error("Reply not found");
  }

  return json(reply);
}

export default function ReplyRoute() {
  const rootData = useRouteLoaderData<RootLoaderTypes>("root");
  const data: PostWithRelations = useLoaderData<LoaderFunction>();
  const errors = useActionData<ActionFunction>();
  const fetcher = useFetcher();

  return (
    <div className="flex flex-col gap-5">
      <Title>Post</Title>
      <Card className="!p-0 !pt-6 !gap-0">
        <Post
          post={data.post}
          userId={rootData?.id}
          child={true}
          reply={true}
        />
        <div className="h-8 pl-[36px]">
          <div
            className={`border ${
              data?.parentReply?.parentReply?.parentReply ? "border-dashed" : ""
            } w-0 h-full`}
          ></div>
        </div>
        <div>
          <Post
            key={data.id}
            post={data}
            userId={rootData?.id}
            rootPostId={data.post.id}
            enlarge={true}
            reply={true}
            topTitle={
              <>
                Replying to
                {data.parentReply ? (
                  <Poster
                    style="compact"
                    username={data.parentReply.author.username}
                    name={data.parentReply.author.name}
                    pfp={data.parentReply.author.pfp}
                  />
                ) : (
                  <Poster
                    style="compact"
                    username={data.post.author.username}
                    name={data.post.author.name}
                    pfp={data.post.author.pfp}
                  />
                )}
              </>
            }
          />
        </div>
        {rootData?.id ? (
          <div className="p-6 !bg-ctp-sky/5">
            <SubTitle>New reply</SubTitle>
            <fetcher.Form
              action="/home"
              className="flex flex-col gap-3"
              method="POST"
            >
              <input type="hidden" name="intent" value={"reply"} />
              <input type="hidden" name="postId" value={data.post.id} />
              <input type="hidden" name="replyId" value={data.id} />
              <FormLabel>
                <Text>Reply body</Text>
                <TextArea name="reply" />
                <Text type="error">{errors?.body ? errors.body : ""}</Text>
              </FormLabel>
              <Button type="submit">Post</Button>
            </fetcher.Form>
          </div>
        ) : (
          ""
        )}
        {data.childReplies.map((reply) => (
          <div key={reply.id}>
            <Post
              post={reply}
              userId={rootData?.id}
              rootPostId={data.post.id}
              reply={true}
              topTitle={
                <>
                  Replying to
                  {data?.parentReply?.author ? (
                    <Poster
                      style="compact"
                      username={data.parentReply.author.username}
                      name={data.parentReply.author.name}
                      pfp={data.parentReply.author.pfp}
                    />
                  ) : (
                    <Poster
                      style="compact"
                      username={data.post.author.username}
                      name={data.post.author.name}
                      pfp={data.post.author.pfp}
                    />
                  )}
                </>
              }
            />
          </div>
        ))}
      </Card>
    </div>
  );
}
