import { redirect } from "@remix-run/node";
import type {
  ActionFunction,
  ActionFunctionArgs,
  LoaderFunctionArgs,
  MetaFunction,
} from "@remix-run/node";
import { Form, useActionData } from "@remix-run/react";
import { commitSession, getSession } from "~/utils/session.server";
import { createUser } from "~/models/user.server";
import { FormInput, FormLabel } from "~/components/Form";
import { Button } from "~/components/Button";
import { Card } from "~/components/Card";
import { Text, Title } from "~/components/Typography";

export const meta: MetaFunction<typeof loader> = () => {
  return [{ title: `Register | Twitter Clone` }];
};

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (session.has("userId")) {
    return redirect("/");
  }

  return null;
}

export async function action({ request }: ActionFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (session.has("userId")) {
    return redirect("/");
  }

  const formData = await request
    .formData()
    .then((data) => Object.fromEntries(data));

  const { id, username, errors } = await createUser(
    String(formData.username),
    String(formData.password),
    String(formData.rpassword)
  );

  if (Object.values(errors).some(Boolean)) {
    return errors;
  }

  session.set("userId", id);
  session.set("username", username);
  session.flash("globalMessage", `Registered and logged in to @${username}`);

  return redirect("/", {
    headers: {
      "Set-Cookie": await commitSession(session),
    },
  });
}

export default function Register() {
  const errors = useActionData<ActionFunction>();

  return (
    <div className="flex flex-col gap-5">
      <Title>Register</Title>
      <Card>
        <Form className="flex flex-col gap-3" method="POST">
          <FormLabel>
            <Text>Username</Text> <FormInput type="text" name="username" />{" "}
            <Text type="error">{errors?.username ? errors.username : ""}</Text>
          </FormLabel>
          <FormLabel>
            <Text>Password</Text> <FormInput type="password" name="password" />{" "}
            <Text type="error">{errors?.password ? errors.password : ""}</Text>
          </FormLabel>
          <FormLabel>
            <Text>Repeat password</Text>{" "}
            <FormInput type="password" name="rpassword" />{" "}
            <Text type="error">
              {errors?.rpassword ? errors.rpassword : ""}
            </Text>
          </FormLabel>
          <Button type="submit">Register</Button>
        </Form>
      </Card>
    </div>
  );
}
