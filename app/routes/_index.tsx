import type { LoaderFunctionArgs, MetaFunction } from "@remix-run/node";
import { redirect } from "@remix-run/node";
import { Text } from "~/components/Typography";
import { getSession } from "~/utils/session.server";

export const meta: MetaFunction<typeof loader> = () => {
  return [{ title: `Welcome | Twitter Clone` }];
};

export async function loader({ request }: LoaderFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (session.has("userId")) {
    return redirect("/home");
  }

  return null;
}

export default function Index() {
  return (
    <div className="rounded-lg bg-ctp-sky/10">
      <div className="flex flex-col gap-1 justify-center items-center h-96">
        <img src="/logo.png" alt="twitter" width={64} />
        <h1 className="text-7xl font-black">
          <span className="text-sky-400">Twitter</span>{" "}
          <span className="">Clone</span>
        </h1>
        <Text>Twitter Clone made with Remix</Text>
      </div>
    </div>
  );
}
