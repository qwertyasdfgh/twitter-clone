import {
  json,
  type LoaderFunction,
  type LoaderFunctionArgs,
  type ActionFunction,
} from "@remix-run/node";
import {
  useActionData,
  useFetcher,
  useLoaderData,
  useRouteLoaderData,
} from "@remix-run/react";
import { Card } from "~/components/Card";
import { SubTitle, Text, Title } from "~/components/Typography";
import type { PostWithRelations } from "~/utils/prisma.server";
import { prisma } from "~/utils/prisma.server";
import { Post, Poster } from "~/components/Post";
import type { RootLoaderTypes } from "~/root";
import { Button } from "~/components/Button";
import { FormLabel, TextArea } from "~/components/Form";

export async function loader({ request, params }: LoaderFunctionArgs) {
  const post = await prisma.post.findUnique({
    where: {
      id: Number(params.id),
    },
    select: {
      id: true,
      userId: true,
      text: true,
      author: {
        select: {
          id: true,
          username: true,
          name: true,
          pfp: true,
        },
      },
      likes: {
        select: {
          id: true,
          userId: true,
        },
      },
      reposts: {
        select: {
          id: true,
          userId: true,
        },
      },
      replies: {
        where: {
          parentReplyId: null,
        },
        select: {
          id: true,
          text: true,
          createdAt: true,
          likes: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          author: {
            select: {
              id: true,
              username: true,
              name: true,
              pfp: true,
            },
          },
          reposts: {
            select: {
              id: true,
              userId: true,
              postId: true,
            },
          },
          post: {
            select: {
              id: true,
              text: true,
              createdAt: true,
              likes: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              reposts: {
                select: {
                  id: true,
                  userId: true,
                  postId: true,
                  user: {
                    select: {
                      username: true,
                      name: true,
                      pfp: true,
                    },
                  },
                },
              },
              author: {
                select: {
                  id: true,
                  username: true,
                  name: true,
                  pfp: true,
                },
              },
              _count: {
                select: {
                  replies: true,
                },
              },
            },
          },
          _count: {
            select: {
              childReplies: true,
            },
          },
        },
        orderBy: {
          likes: {
            _count: "desc",
          },
        },
      },
      createdAt: true,
      _count: {
        select: {
          replies: {
            where: {
              parentReplyId: null,
            },
          },
        },
      },
    },
  });

  if (!post) {
    throw Error("Post not found");
  }

  return json(post);
}

export default function PostRoute() {
  const rootData = useRouteLoaderData<RootLoaderTypes>("root");
  const data: PostWithRelations = useLoaderData<LoaderFunction>();
  const errors = useActionData<ActionFunction>();
  const fetcher = useFetcher();

  return (
    <div className="flex flex-col gap-5">
      <Title>Post</Title>
      <Card className="!p-0 !gap-0">
        <div className="border-b">
          <Post
            key={data.id}
            post={data}
            enlarge={true}
            userId={rootData?.id}
          />
        </div>
        {rootData?.id ? (
          <div className="p-6 !bg-ctp-sky/5">
            <SubTitle>New reply</SubTitle>
            <fetcher.Form
              action="/home"
              className="flex flex-col gap-3"
              method="POST"
            >
              <input type="hidden" name="intent" value={"reply"} />
              <input type="hidden" name="postId" value={data.id} />
              <FormLabel>
                <Text>Reply body</Text>
                <TextArea name="reply" />
                <Text type="error">{errors?.body ? errors.body : ""}</Text>
              </FormLabel>
              <Button type="submit">Post</Button>
            </fetcher.Form>
          </div>
        ) : (
          ""
        )}
        {data.replies.map((reply) => (
          <div key={reply.id}>
            <Post
              post={reply}
              userId={rootData?.id}
              rootPostId={data.id}
              reply={true}
              topTitle={
                <Text className="flex gap-1" type="subtitle">
                  Replying to{" "}
                  <Poster
                    style="compact"
                    username={data.author.username}
                    name={data.author.name}
                    pfp={data.author.pfp}
                  />
                </Text>
              }
            />
          </div>
        ))}
      </Card>
    </div>
  );
}
