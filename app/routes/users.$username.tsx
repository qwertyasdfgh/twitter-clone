import type {
  ActionFunction,
  LoaderFunction,
  LoaderFunctionArgs,
  MetaFunction,
} from "@remix-run/node";
import {
  Form,
  useActionData,
  Link,
  useLoaderData,
  useRouteError,
  useRouteLoaderData,
} from "@remix-run/react";
import { Button } from "~/components/Button";
import { Card } from "~/components/Card";
import { Post, Poster } from "~/components/Post";
import { SubTitle, Text, Title } from "~/components/Typography";
import { followUser, unFollowUser } from "~/models/user.server";
import type { RootLoaderTypes } from "~/root";
import { prisma, type UserWithRelations } from "~/utils/prisma.server";
import { getSession } from "~/utils/session.server";

export const meta: MetaFunction<typeof loader> = ({ data, error }) => {
  return [{ title: `${error ? "Not Found" : data?.username} | Twitter Clone` }];
};

export async function action({ request }: LoaderFunctionArgs) {
  const session = await getSession(request.headers.get("Cookie"));
  if (!session.has("userId")) {
    return;
  }

  const formData = await request
    .formData()
    .then((data) => Object.fromEntries(data));

  switch (formData.intent) {
    case "follow":
      const followErrors = await followUser(
        Number(formData.id),
        session.get("userId")
      );

      if (Object.values(followErrors).some(Boolean)) {
        return followErrors;
      }

      break;
    case "unfollow":
      const unFollowErrors = await unFollowUser(
        Number(formData.id),
        session.get("userId")
      );

      if (Object.values(unFollowErrors).some(Boolean)) {
        return unFollowErrors;
      }

      break;
    default:
      break;
  }

  return null;
}

export async function loader({ request, params }: LoaderFunctionArgs) {
  const url = new URL(request.url);
  const tab = url.searchParams.get("tab") || null;

  const data = await prisma.user.findFirst({
    where: {
      username: params.username,
    },
    select: {
      id: true,
      username: true,
      name: true,
      pfp: true,
      desc: true,
      createdAt: true,
      posts:
        !tab || tab === "posts"
          ? {
              select: {
                id: true,
                text: true,
                createdAt: true,
                likes: {
                  select: {
                    id: true,
                    userId: true,
                    postId: true,
                    user: {
                      select: {
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                  },
                },
                reposts: {
                  select: {
                    id: true,
                    userId: true,
                    postId: true,
                    user: {
                      select: {
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                  },
                },
                author: {
                  select: {
                    id: true,
                    username: true,
                    name: true,
                    pfp: true,
                  },
                },
                _count: {
                  select: {
                    replies: {
                      where: {
                        parentReplyId: null,
                      },
                    },
                  },
                },
              },
              orderBy: {
                createdAt: "desc",
              },
            }
          : false,
      likes:
        tab === "likes"
          ? {
              select: {
                id: true,
                userId: true,
                postId: true,
                createdAt: true,
                user: {
                  select: {
                    username: true,
                    name: true,
                    pfp: true,
                  },
                },
                post: {
                  select: {
                    id: true,
                    text: true,
                    createdAt: true,
                    likes: {
                      select: {
                        id: true,
                        userId: true,
                        postId: true,
                        user: {
                          select: {
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    reposts: {
                      select: {
                        id: true,
                        userId: true,
                        postId: true,
                        user: {
                          select: {
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    author: {
                      select: {
                        id: true,
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                    _count: {
                      select: {
                        replies: {
                          where: {
                            parentReplyId: null,
                          },
                        },
                      },
                    },
                  },
                },
                reply: {
                  select: {
                    id: true,
                    text: true,
                    createdAt: true,
                    author: {
                      select: {
                        id: true,
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                    likes: {
                      select: {
                        id: true,
                        userId: true,
                      },
                    },
                    reposts: {
                      select: {
                        id: true,
                        userId: true,
                      },
                    },
                    parentReply: {
                      select: {
                        author: {
                          select: {
                            id: true,
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    post: {
                      select: {
                        author: {
                          select: {
                            id: true,
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    _count: {
                      select: {
                        childReplies: {
                          where: {
                            parentReplyId: null,
                          },
                        },
                      },
                    },
                  },
                },
              },
              orderBy: {
                createdAt: "desc",
              },
            }
          : false,
      reposts:
        tab === "reposts"
          ? {
              select: {
                id: true,
                userId: true,
                postId: true,
                createdAt: true,
                user: {
                  select: {
                    username: true,
                    name: true,
                    pfp: true,
                  },
                },
                post: {
                  select: {
                    id: true,
                    text: true,
                    createdAt: true,
                    likes: {
                      select: {
                        id: true,
                        userId: true,
                        postId: true,
                        user: {
                          select: {
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    reposts: {
                      select: {
                        id: true,
                        userId: true,
                        postId: true,
                        user: {
                          select: {
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    author: {
                      select: {
                        id: true,
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                    _count: {
                      select: {
                        replies: {
                          where: {
                            parentReplyId: null,
                          },
                        },
                      },
                    },
                  },
                },
                reply: {
                  select: {
                    id: true,
                    text: true,
                    createdAt: true,
                    author: {
                      select: {
                        id: true,
                        username: true,
                        name: true,
                        pfp: true,
                      },
                    },
                    likes: {
                      select: {
                        id: true,
                        userId: true,
                      },
                    },
                    reposts: {
                      select: {
                        id: true,
                        userId: true,
                      },
                    },
                    parentReply: {
                      select: {
                        author: {
                          select: {
                            id: true,
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    post: {
                      select: {
                        author: {
                          select: {
                            id: true,
                            username: true,
                            name: true,
                            pfp: true,
                          },
                        },
                      },
                    },
                    _count: {
                      select: {
                        childReplies: {
                          where: {
                            parentReplyId: null,
                          },
                        },
                      },
                    },
                  },
                },
              },
              orderBy: {
                createdAt: "desc",
              },
            }
          : false,
      followedBy: {
        select: {
          id: true,
        },
      },
      _count: {
        select: {
          followedBy: true,
          following: true,
        },
      },
    },
  });

  if (!data) {
    throw Error(`User ${params.username} not found`);
  }

  return data;
}

export default function UserProfile() {
  const rootData = useRouteLoaderData<RootLoaderTypes>("root");
  const data: UserWithRelations = useLoaderData<LoaderFunction>();
  const actionData = useActionData<ActionFunction>();
  const isFollowing =
    data.followedBy.filter((follow) => follow.id === rootData?.id).length > 0;
  console.log(data);

  return (
    <div className="flex flex-col gap-5">
      <Title>Profile</Title>
      <Card>
        <div className="flex w-full">
          <div className="flex flex-1 gap-2 items-center">
            <Poster pfp={data.pfp} username={data.username} name={data.name} />
          </div>
          {rootData?.id && rootData?.id !== data.id ? (
            <div>
              <Form method="POST">
                <input
                  type="hidden"
                  name="intent"
                  value={isFollowing ? "unfollow" : "follow"}
                />
                <input type="hidden" name="id" value={data.id} />
                <Button type="submit">
                  {isFollowing ? "Following!" : "Follow"}
                </Button>
              </Form>
            </div>
          ) : (
            ""
          )}
        </div>
        {data?.desc ? (
          <div>
            <Text>{data.desc}</Text>
          </div>
        ) : (
          ""
        )}
        <div className="flex gap-5">
          <div className="flex gap-2 items-center">
            <Text type="subtitle">Followers:</Text>
            <Link to={`/users/${data.username}/followers`}>
              <Text type="link">{data._count.followedBy}</Text>
            </Link>
          </div>
          <div className="flex gap-2 items-center">
            <Text type="subtitle">Follows:</Text>
            <Link to={`/users/${data.username}/following`}>
              <Text type="link">{data._count.following}</Text>
            </Link>
          </div>
        </div>
      </Card>
      <div className="flex gap-5">
        <Link to={`?tab=posts`}>
          <SubTitle>Posts</SubTitle>
        </Link>
        <Link to={`?tab=likes`}>
          <SubTitle>Likes</SubTitle>
        </Link>
        <Link to={`?tab=reposts`}>
          <SubTitle>Reposts</SubTitle>
        </Link>
      </div>
      <div className="flex flex-col gap-10 lg:flex-row">
        {data.posts ? (
          <div className="flex flex-col flex-grow gap-5 w-full">
            {data.posts.length > 0 ? (
              <Card className="!p-0 !gap-0 divide-y">
                {data.posts.map((post) => (
                  <Post userId={rootData?.id} key={post.id} post={post} />
                ))}
              </Card>
            ) : (
              <Text type="subtitle">No posts yet</Text>
            )}
          </div>
        ) : (
          ""
        )}
        {data.likes ? (
          <div className="flex flex-col flex-grow gap-5 w-full">
            {data.likes.length > 0 ? (
              <Card className="!p-0 !gap-0 divide-y">
                {data.likes.map((like) => (
                  <Post
                    userId={rootData?.id}
                    key={like.id}
                    rootPostId={like?.reply?.post?.id}
                    post={like.reply ? like.reply : like.post}
                    topTitle={
                      <>
                        <strong>{data.username}</strong> liked this
                        {like.reply ? (
                          <>
                            , Replying to{" "}
                            {like.reply.parentReply ? (
                              <Poster
                                style="compact"
                                username={
                                  like.reply.parentReply.author.username
                                }
                                name={like.reply.parentReply.author.name}
                                pfp={like.reply.parentReply.author.pfp}
                              />
                            ) : (
                              <Poster
                                style="compact"
                                username={like.reply.post.author.username}
                                name={like.reply.post.author.name}
                                pfp={like.reply.post.author.pfp}
                              />
                            )}
                          </>
                        ) : (
                          ""
                        )}
                      </>
                    }
                  />
                ))}
              </Card>
            ) : (
              <Text type="subtitle">No likes yet</Text>
            )}
          </div>
        ) : (
          ""
        )}
        {data.reposts ? (
          <div className="flex flex-col flex-grow gap-5 w-full">
            {data.reposts.length > 0 ? (
              <Card className="!p-0 !gap-0 divide-y">
                {data.reposts.map((repost) => (
                  <Post
                    userId={rootData?.id}
                    key={repost.id}
                    rootPostId={repost?.reply?.post?.id}
                    post={repost.reply ? repost.reply : repost.post}
                    topTitle={
                      <>
                        <strong>{data.username}</strong> reposted this
                        {repost.reply ? (
                          <>
                            , Replying to{" "}
                            {repost.reply.parentReply ? (
                              <Poster
                                style="compact"
                                username={
                                  repost.reply.parentReply.author.username
                                }
                                name={repost.reply.parentReply.author.name}
                                pfp={repost.reply.parentReply.author.pfp}
                              />
                            ) : (
                              <Poster
                                style="compact"
                                username={repost.reply.post.author.username}
                                name={repost.reply.post.author.name}
                                pfp={repost.reply.post.author.pfp}
                              />
                            )}
                          </>
                        ) : (
                          ""
                        )}
                      </>
                    }
                  />
                ))}
              </Card>
            ) : (
              <Text type="subtitle">No reposts yet</Text>
            )}
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}

export function ErrorBoundary() {
  const error = useRouteError();

  return (
    <>
      <Title>Error</Title>
      <Text>{error.message}</Text>
    </>
  );
}
