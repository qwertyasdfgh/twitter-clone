FROM node:20-alpine
RUN apk add --no-cache openssl
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable
WORKDIR /app

COPY ./package.json ./pnpm-lock.yaml ./
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile
COPY ./ .
RUN pnpm run build
RUN pnpm prisma generate
RUN pnpm prisma migrate deploy

EXPOSE 3000
CMD ["pnpm", "run" ,"start"]
