﻿# Twitter Clone

A literal Twitter Clone. It is made with Remix, Prisma and Tailwind CSS.

## Running for dev or prod

Clone the repo, install dependencies, put secrets in .env, run the migration and start.

```
git clone https://git.disroot.org/qwertyasdfgh/twitter-clone
pnpm install
cp .env.example .env // edit the file!!!
pnpm prisma migrate deploy/dev // deploy if prod, dev if dev
pnpm prisma generate
pnpm build // if prod
pnpm start/dev // start if prod, dev if dev
```

Run the app behind a reverse proxy on prod

## Docker

Copy .env.example as .env and then just execute `docker compose up -d`
