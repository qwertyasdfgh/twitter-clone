-- CreateTable
CREATE TABLE "_UserReposts" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL,
    CONSTRAINT "_UserReposts_A_fkey" FOREIGN KEY ("A") REFERENCES "Post" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT "_UserReposts_B_fkey" FOREIGN KEY ("B") REFERENCES "User" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "_UserReposts_AB_unique" ON "_UserReposts"("A", "B");

-- CreateIndex
CREATE INDEX "_UserReposts_B_index" ON "_UserReposts"("B");
